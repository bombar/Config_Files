#!/bin/bash

MIXER="hw:1"
SCONTROL=Master
STEP=1

format() {
  perl_filter='if (/.*\[(\d+%)\] (\[(-?\d+.\d+dB)\] )?\[(on|off)\]/)'
  perl_filter+='{CORE::say $4 eq "off" ? "MUTE" : "'
  # If dB was selected, print that instead
  perl_filter+=$([[ $STEP = *dB ]] && echo '$3' || echo '$1')
  perl_filter+='"; exit}'
  perl -ne "$perl_filter"
}

volume() {
  amixer -D $MIXER get $SCONTROL
}

volume | format
