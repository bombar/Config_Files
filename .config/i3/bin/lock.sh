#! /bin/sh

# Create a screenshot of background, blur it, and pass it to i3lock as background image

scrot -e 'convert $f -blur 20x20 /tmp/lock.png' /tmp/lock.png && i3lock -c 000000 -i /tmp/lock.png && rm /tmp/lock.png
