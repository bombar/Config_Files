#!/usr/bin/env python3

# Slide current workspace to the right on i3-bar

import i3ipc
from argparse import ArgumentParser
import logging
import colorlog

logger = logging.getLogger()


def output_workspace_mapping(i3):
    """
    Take a connection to i3 socket in input and
    Return a dictionnary where:
        - keys are the different output names (=Display Screens)
        - values are the names of the workspaces on this output.
    """
    workspaces = i3.get_workspaces()
    d = {}
    for w in workspaces:
        output = w.output
        ow = d.get(output, [])
        ow.append(w.name)
        d[output] = ow
    return d


def get_output_from_workspace(i3, workspace):
    """
    Return the name of the output of which the workspace belongs.
    """
    owmap = output_workspace_mapping(i3)
    for o, ws in owmap.items():
        if workspace in ws:
            logger.info("[get_output_from_workspace]")
            logger.info(f"  ⤷ Workspace \"{workspace}\" "
                        f"is on output \"{o}\"")
            return o


def get_workspace_from_name(i3, name):
    """
    Return the first workspace object of given name.
    """
    workspaces = i3.get_workspaces()
    return list(filter(lambda w: w.name == name, workspaces))[0]


def swap_workspaces(i3, w1, w2):
    w1 = get_workspace_from_name(i3, w1)
    w2 = get_workspace_from_name(i3, w2)
    if w1.name == w2.name:
        logger.debug("[swap_workspaces]: Nothing to do.")
        return
    try:
        num1, name1 = w1.name.split(":")
    except ValueError:
        num1, name1 = w1.num, w1.name
    try:
        num2, name2 = w2.name.split(":")
    except ValueError:
        num2, name2 = w2.num, w2.name
    logger.info("[swap_workspaces]")
    logger.info(f"⤷ i3-msg 'rename workspace "
                f"\"{w1.name}\" to \"{num2}:{name1}\"'")
    answers = i3.command(f'rename workspace "{w1.name}" to "{num2}:{name1}"')
    for ans in answers:
        if ans.error:
            logger.critical(f"Error in [swap_workspace]: {ans.error}")
    logger.info("[swap_workspaces]")
    logger.info(f"⤷ i3-msg 'rename workspace "
                f"\"{w2.name}\" to \"{num1}:{name2}\"")
    answers = i3.command(f'rename workspace "{w2.name}" to "{num1}:{name2}"')
    for ans in answers:
        if ans.error:
            logger.critical(f"Error in [swap_workspace]: {ans.error}")


def slide_workspace(i3, workspace, output, direction):
    """
    Slide the workspace to the specified direction (Left or Right)
    on given output. Workspace MUST be on given output.
    """
    owmap = output_workspace_mapping(i3)
    workspaces = owmap.get(output)
    pos = workspaces.index(workspace)
    if direction == "right":
        logger.debug(f"Slide workspace \"{workspace}\" to the right.")
        nextworkspace = workspaces[(pos + 1) % len(workspaces)]
    elif direction == "left":
        logger.debug(f"Slide workspace \"{workspace}\" to the left.")
        nextworkspace = workspaces[pos - 1]
    else:
        nextworkspace = workspaces[pos]
    swap_workspaces(i3, workspace, nextworkspace)


def main(direction):
    i3 = i3ipc.Connection()
    focused = i3.get_tree().find_focused()
    current_workspace = focused.workspace().name
    current_output = get_output_from_workspace(i3, current_workspace)
    slide_workspace(i3, current_workspace, current_output, direction)


if __name__ == "__main__":
    format_string = "%(asctime)s - %(levelname)s - %(message)s"
    stdout_handler = logging.StreamHandler()
    stdout_formatter = colorlog.ColoredFormatter(
        "%(log_color)s{}".format(format_string))
    stdout_handler.setFormatter(stdout_formatter)
    logger.addHandler(stdout_handler)

    parser = ArgumentParser()
    parser.add_argument("direction", help="Left or Right.")
    parser.add_argument("-v", "--verbose",
                        help="Verbose mode.",
                        action="store_true")
    args = parser.parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)

    direction = args.direction.lower()
    main(direction)
