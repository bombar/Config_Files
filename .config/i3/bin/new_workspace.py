#!/usr/bin/python3

# Create a new workspace with the lowest available number


import i3ipc
from argparse import ArgumentParser
import logging
import colorlog

logger = logging.getLogger()


def main():
    i3 = i3ipc.Connection()
    workspaces = i3.get_workspaces()
    all_numbers = sorted(list(set(map(lambda w: w.num, workspaces))))
    logger.info(f"Current workspaces are {all_numbers}.")
    new = all_numbers[0]
    i = 0
    while i < len(all_numbers) and (all_numbers[i] < 0 or new == all_numbers[i]):
        new += 1
        i += 1
    logger.info(f"i3-msg 'workspace {new}'")
    i3.command(f"workspace {new}")


if __name__ == "__main__":
    format_string = "%(asctime)s - %(levelname)s - %(message)s"
    stdout_handler = logging.StreamHandler()
    stdout_formatter = colorlog.ColoredFormatter(
        "%(log_color)s{}".format(format_string)
    )
    stdout_handler.setFormatter(stdout_formatter)
    logger.addHandler(stdout_handler)

    parser = ArgumentParser()
    parser.add_argument("-v", "--verbose", help="More logging.", action="store_true")
    args = parser.parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)

    main()
