#!/usr/bin/env bash

if [ ! -d ~/.config/i3/venv ]; then
    python3 -m venv ~/.config/i3/venv --system-site-packages;
    source ~/.config/i3/venv/bin/activate;
    pip3 install -r ~/.config/i3/requirements.txt;
fi

source ~/.config/i3/venv/bin/activate;
