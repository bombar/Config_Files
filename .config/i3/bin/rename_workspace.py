#!/usr/bin/python3

# Rename current workspace and keep the number


import i3ipc
from argparse import ArgumentParser
import logging
import colorlog

logger = logging.getLogger()


def main(newname=""):
    i3 = i3ipc.Connection()
    focused = i3.get_tree().find_focused()
    num, name = None, newname
    if ":" in newname:
        num, name = newname.split(":")
        try:
            num = int(num)
        except ValueError:
            num, name = None, newname
    if num is None:
        num = focused.workspace().num
    logger.info(f"i3-msg 'rename workspace to \"{num}\":\"{name}\"'")
    i3.command(f'rename workspace to "{num}:{name}"')


if __name__ == '__main__':
    format_string = "%(asctime)s - %(levelname)s - %(message)s"
    stdout_handler = logging.StreamHandler()
    stdout_formatter = colorlog.ColoredFormatter(
        "%(log_color)s{}".format(format_string))
    stdout_handler.setFormatter(stdout_formatter)
    logger.addHandler(stdout_handler)

    parser = ArgumentParser()
    parser.add_argument("newname", type=str, nargs="+",
                        help="The new name of the workspace.")
    parser.add_argument("-v", "--verbose",
                        help="More logging.",
                        action="store_true")
    args = parser.parse_args()

    if args.verbose:
        logger.setLevel(logging.DEBUG)

    newname = " ".join(args.newname)
    main(newname)
