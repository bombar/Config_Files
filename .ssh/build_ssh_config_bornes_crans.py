#!/usr/bin/python3
# -*- coding: utf-8 -*-

# La liste des bornes est dans le fichier liste_bornes_crans

def get_bornes():
    with open('liste_bornes_crans', 'r') as bornes:
        l = bornes.readlines()
    return list(filter(any, map(lambda s: s.split('#'), l)))


template = """Host {littleName} {name} {hostname}
   HostName {hostname}
   User root
   ForwardAgent yes
   ProxyJump gulp.crans.org

"""

def main():
    bornes = get_bornes()
    with open('config_bornes_crans', 'w') as config_bornes:
        config_bornes.write("""# .ssh/config_bornes_crans pour les bornes du Crans

# +----------------------+
# | Bornes wifi du Cr@ns |
# +----------------------+
# Accessible aux apprentis via la clé ssh dans cranspasswords ; bornes_key\n\n""")
        for borneCom in bornes:
            if len(borneCom) == 1:
                borne = borneCom[0]
                comm = ''
            else:
                borne, comm = borneCom
            if not borne:
                continue
            name = borne.split('.crans')[0]
            littleName = name.split('.borne')[0]
            hostname = borne.split('\n')[0]
            config_bornes.write('\n#Au{}'.format(comm)*bool(borne and comm)+template.format(littleName=littleName, name=name, hostname=hostname))


if __name__=="__main__":
    main()
