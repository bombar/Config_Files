# .bash_aliases générique mis à votre disposition
# par les gentils membres actifs du Cr@ns
# Vous pouvez l'utiliser, le redistribuer, le modifier à votre convenance.
# Des questions, des suggestions : {nounou,ca}@lists.crans.org
# Licence : WTFPL

# Les sections commentées par #~# sont des features qui ne sont pas activées
# par défaut. Sentez-vous libre de les décommenter pour les utiliser.

#------------------------------------------------------------------------------

# +----------------+
# | Aliases commun |
# +----------------+

# Demander confirmation avant écrasement ou suppression des fichiers/dossiers
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp -i'

# Afficher la taille des partitions en human-readable
alias df='df -h'

# Affichage des couleurs automatiques
if [ -x /usr/bin/dircolors ]; then
    alias grep='grep --color=auto'
    alias egrep='egrep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias ls='ls --color=auto'
fi

# Alias ls
alias ll='ls -alFh'
alias la='ls -A'
alias l='ls -CF'

# parce que LS/sl c'est cool, mais qu'on veut pouvoir l'interrompre
alias sl='sl -e'
alias LS='LS -e'


# +------------+
# | My aliases |
# +------------+

alias sshfsinfo='sshfs bombar@ssh.dptinfo.ens-cachan.fr:/users/dptinfo/bombar ~/Home_411'
alias rm='rm -i'
alias open='gnome-open'
alias irc='ssh -t bombar@zamok.crans.org "~/screen.sh"'
alias zamok='ssh zamok'
alias google='chromium'
alias sl='sl-h'
alias tunnel='ssh -D 1080 note'
alias thot='ssh bombar@thot.crans.org'
alias note='ssh note'
alias note-test='ssh note-test'
alias sauron='ssh sauron'
alias clean='rm -f *.{aux,log,out,bbl,blg,rip,nav,snm,toc,vrb} *~ \#*'
alias cranspasswords='~/Crans/Cranspasswords/cranspasswords'

# +-----+
# | ssh |
# +-----+

# Pour que ssh-add ne mémorise la passphrase que pendant 30 minutes
alias sshadd='ssh-add -t 1800'

#~# # Tunnels (pour les membres actifs)
#~# alias sshimprimante="echo -e 'Port : 2222\nRappel : https://localhost:2222';ssh -L 2222:imprimante.adm.crans.org:443 vo"
#~# alias sshcups="echo -e 'Port : 3333\nRappel : http://localhost:3333';ssh -L 3333:o2.adm.crans.org:631 vo"
#~# alias sshbabar="echo -e 'Port : 3333\nRappel : http://localhost:3333/backuppc';ssh -L 3333:babar.adm.crans.org:80 vo"



# +------------------+
# | Fixing mistyping |
# +------------------+
#~# # Souvenir de Windows
#~# alias cd..='cd ..'

#~# alias screen-r='screen -r'



#~# # +----------------------+
#~# # | AltGr+Space is wrong |
#~# # +----------------------+
#~#
#~# # Workaround crade :
#~# # Permet d'aliaser les commandes dans lesquelles on pipe usuellement
#~# # pour qu'elle marche aussi quand elles sont précédées d'une espace insécable.
#~#
#~# for commande in grep egrep fgrep wc tail head less sed awk xargs sort uniq sponge
#~# do
#~#   alias  $commande=$commande
#~# done
#~# unset commande

# +--------------------------------------+
# | Gestion de variables d'environnement |
# +--------------------------------------+

#~# # Pour loader en mémoire la variable qui empêche less de breaker les lignes
#~# # très utile pour les sorties psql
alias exportless="export LESS='-SR'"


# +-------+
# | softs |
# +-------+
# Des raccourcis pour appeler des logiciels avec des options supplémentaires

#~# # Pour que youtube-dl récupère seulement l'audio, et en MP3 s'il vous plait
#~# alias youtube-dl='youtube-dl --extract-audio -t --audio-format=mp3'

#~# # Pour avoir les diff sous format git-like
#~# alias diff='diff -u'
#~# alias colordiff='colordiff -u'

#~# # Pour que mtr n'utilise pas l'interface graphique
#~# alias mtr='mtr -t'

#~# # Pour que les semaines de cal commencent le Lundi
#~# alias cal='ncal -bM'

#~# # Hack pour déclencher les hooks sur un dépôt bare git
#~# # (genre git-update-server-info et/ou trigger KGB)
#~# # en faisant un truc bidon et transparent pour l'historique
#~# # (à savoir créer une branche et la supprimer)
#~# alias shakegit='git checkout -b shakegitbranch && git push origin shakegitbranch && git checkout master && git branch -d shakegitbranch && git push origin :shakegitbranch'

# +--------+
# | Divers |
# +--------+

#~# # Pour se débarasser rapidement des fichier garbage
#~# # C'est typiquement là où il faut rajouter vos extensions persos !
#~# alias rmtilde='rm -f *~ .*~'
#~# alias rm~=rmtilde
#~# alias rmpyc='rm -f *.pyc'
#~# alias rmccompiled='rm -f *.o *.h'
#~# alias rmso='rm -f *.so'
#~# alias rmclass='rm -f *.class'

#~# # Pour récupérer les droits d'un fichier en octal
#~# alias getmod='/usr/bin/stat -c "%a"'

# Pour trouver les pages des pdfs du dossier courrant
alias pdfpages="find . -name '*.pdf' -exec pdfinfo {} \; | egrep '^Pages'"
# et pour les sommer
alias pdfpagessum='pdfpages | awk "{print \$2}" | paste -sd+ | bc'

# Add an "alert" alias for long running commands.  Use like so:
#   long_command; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Pour afficher les derniers mails reçus
# (adapter le path de votre procmail.log si besoin)
alias tailprocmail='tail -f ~/.procmail/logs/procmail.log'



# +------------+
# | Cool stuff |
# +------------+

# A♡ !!!
alias a♡tv='cvlc udp://@239.231.140.230:1337'

#Meteo
meteo () { curl fr.wttr.in/`tr -s ' ' '_' <<< "$*"`; }

#Lune
moon () { curl fr.wttr.in/Moon; }
