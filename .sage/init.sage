# To use more readable colors
%colors Linux

# Ignore deprecation warnings from completion. Will be fixed in Ipython 7.15 ?
warnings.filterwarnings('ignore', category=DeprecationWarning, module='jedi')
