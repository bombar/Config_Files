" .vimrc générique mis à votre disposition
" par les gentils membres actifs du Cr@ns
" Vous pouvez l'utiliser, le redistribuer, le modifier à votre convenance.
" Des questions, des suggestions : {nounou,ca}@lists.crans.org
" Licence : WTFPL

" Les sections commentées par "~" sont des features qui ne sont pas activées
" par défaut. Sentez-vous libre de les décommenter pour les utiliser.

"------------------------------------------------------------------------------

"~" " Pour charger de la configuration avant la configuration crans
"~" source ~/.vimrc_before

" +-----------+
" | Affichage |
" +-----------+

" Affiche la commande en train d'être tapée en bas à droite de l'écran
set showcmd

" Affiche le nom du fichier et le chemin dans le titre du terminal
set title

"~" " N'affiche pas sur plusieurs lignes les lignes qui sont plus grandes que la fenêtre (comme sous nano)
"~" set nowrap

"~" " Montre brièvement la paire de parenthèse lors de sa fermeture
"~" set showmatch

"~" " Optimise la coloration de vim pour l'adapter à un terminal sombre (par défaut 'light')
"~" set background=dark

" ~~ Numérotation ~~
" Affiche 'ligne,n de caractère,colonne' en bas à droite du terminal
set ruler

" Affiche les numéros de lignes à gauche du terminal
set number

" Retire la marge à gauche sur les numéros de lignes
set numberwidth=2

"~" " Surligne la ligne du curseur
"~" set cursorline

"~" " Surligne la colonne du curseur
"~" set cursorcolumn

" +-----------------+
" | Édition de code |
" +-----------------+

" ~~ Coloration syntaxique ~~
" Active la coloration syntaxique pour le type de fichier détecté
if has("syntax")
  syntax on
endif

" ~~ Indentation et tabulation ~~
" Active les régles d'indentation pour le type de fichier détecté
if has("autocmd")
  filetype plugin indent on
endif

"~" " Conserve l'indentation de la ligne précédente lors d'un retour à la ligne
"~" set autoindent

"~" " Ne suprime pas l'indentation faite par 'autoindent' si la ligne est laissé vide
"~" set cpoptions+=I

"~" " Met les tabulations à 4 colonnes
"~" set tabstop=4

"~" " Indente à 4 colonnes pour les opérations de réindentation
"~" set shiftwidth=4
"~" set shiftwidth=4 " pour <<, >> et ==

"~" " Remplace les (futures) tabulations par des espaces
"~" set expandtab
"~" " On veut des tabulations dans les MakeFiles
"~" autocmd FileType make set noexpandtab tabstop=4 shiftwidth=2 nopi ci

"~" " Ctrl-p change entre le mode paste et nopaste
"~" set pastetoggle=<C-P>

"~" " Utilise le presse papier system (nécessite vim-gnome sous Debian)
"~" set clipboard^=unnamedplus

"~" "Les fichiers .ssh/config_* sont considérés comme des fichiers de
"~" "configuration pour ssh.
"~" au BufNewFile,BufRead */.ssh/config_*  setf sshconfig

" +---------------------------+
" | Recherche et substitution |
" +---------------------------+

" ~~ Recherche ~~
" Commence la recherche dès les premiers caractères tapés (comme sous less)
set incsearch

"~" " Surligne les correspondances dans les recherches
"~" " (Exécuter nohl pour désactiver les hl (ils restent même une fois la recherche terminée))
"~" set hlsearch

"~" " Rend la recherche insensible aux majuscules et minuscules, sauf si le
"~" " motif contient une majuscule
"~" set ignorecase
"~" set smartcase

" +-----------------------------+
" | Enregistrement et fermeture |
" +-----------------------------+

"~" " Enregistre automatiquement pour certaines commandes, notament make (voir :help autowrite)
"~" set autowrite

"~" " Met à jour automatiquement les fichiers modifiés hors de vim
"~" set autoread

"~" " Pose la question au lieu d'échouer lors de la fermeture quand il y des
"~" " modifications non sauvées et que ! n'est pas précisé.
"~" set confirm

" +--------+
" | Divers |
" +--------+

" Change la taille de l'historique des commandes (par défaut 20)
set history=50

"~" " Change le comportement de la TAB-complétion : on complète au plus long
"~" " au premier appui, on affiche la liste des possibilités au deuxième
"~" set wildmode=longest,list

"~" " Active l'utilisation de la souris. (Ne marche qu'avec certains terminaux voir :help mouse)
"~" set mouse=a

"~" " Permet aux flèches droites et gauches de déplacer le curseur au début de
"~" " de la prochaine ligne ou à la fin de la précédente ligne
"~" set whichwrap=b,s,<,>,[,]

" +------------------+
" | Hacks et scripts |
" +------------------+

" ~~ Hack pour mettre en rouges les espaces indésirables en fin de ligne. ~~
" ~~ Ne gêne pas la vue en mode édition. ~~
" ~~ Adapté de http://vim.wikia.com/wiki/Highlight_unwanted_spaces ~~

"~" highlight EspaceFinLigne ctermbg=red guibg=red
"~" match EspaceFinLigne /\s\+$/
"~" autocmd BufWinEnter * match EspaceFinLigne /\s\+$/
"~" autocmd InsertEnter * match EspaceFinLigne /\s\+\%#\@<!$/
"~" autocmd InsertLeave * match EspaceFinLigne /\s\+$/
"~" autocmd BufWinLeave * call clearmatches()

" ~~ Coloration du fond après n colonnes ~~
" ~~ /!\ Seulement pour vim 7.3 et plus /!\ ~~
" ~~ Adapté de http://blog.hanschen.org ~~

"~" highlight ColorColumn ctermbg=DarkGrey guibg=DarkGrey
"~" if exists('+colorcolumn')
"~"     execute "set colorcolumn=".join(range(81,335), ',')
"~" endif

"~" " Surligne les espaces insécables
"~" au BufEnter * hi Nbsp ctermbg=233 guibg=black
"~" au BufEnter * match Nbsp /\%uA0/

" +-------+
" | Perso |
" +-------+

"~" " Pour charger de la configuration après la configuration crans
"~" source ~/.vimrc_after

" ## added by OPAM user-setup for vim / base ## 93ee63e278bdfc07d1139a748ed3fff2 ## you can edit, but keep this line
let s:opam_share_dir = system("opam config var share")
let s:opam_share_dir = substitute(s:opam_share_dir, '[\r\n]*$', '', '')

let s:opam_configuration = {}

function! OpamConfOcpIndent()
  execute "set rtp^=" . s:opam_share_dir . "/ocp-indent/vim"
endfunction
let s:opam_configuration['ocp-indent'] = function('OpamConfOcpIndent')

function! OpamConfOcpIndex()
  execute "set rtp+=" . s:opam_share_dir . "/ocp-index/vim"
endfunction
let s:opam_configuration['ocp-index'] = function('OpamConfOcpIndex')

function! OpamConfMerlin()
  let l:dir = s:opam_share_dir . "/merlin/vim"
  execute "set rtp+=" . l:dir
endfunction
let s:opam_configuration['merlin'] = function('OpamConfMerlin')

let s:opam_packages = ["ocp-indent", "ocp-index", "merlin"]
let s:opam_check_cmdline = ["opam list --installed --short --safe --color=never"] + s:opam_packages
let s:opam_available_tools = split(system(join(s:opam_check_cmdline)))
for tool in s:opam_packages
  " Respect package order (merlin should be after ocp-index)
  if count(s:opam_available_tools, tool) > 0
    call s:opam_configuration[tool]()
  endif
endfor
" ## end of OPAM user-setup addition for vim / base ## keep this line
