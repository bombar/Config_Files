(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(inhibit-startup-screen t)
 '(package-selected-packages
   (quote
    (fstar-mode markdown-mode+ markdown-mode yaml-mode jedi company-coq))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
;; Add opam emacs directory to the load-path
(setq opam-share (substring (shell-command-to-string "opam config var share 2> /dev/null") 0 -1))
(add-to-list 'load-path (concat opam-share "/emacs/site-lisp"))
;; Load merlin-mode
(require 'merlin)
;; Start merlin on ocaml files
(add-hook 'tuareg-mode-hook 'merlin-mode t)
(add-hook 'caml-mode-hook 'merlin-mode t)
;; Enable auto-complete
(setq merlin-use-auto-complete-mode 'easy)
;; Use opam switch to lookup ocamlmerlin binary
(setq merlin-command 'opam)
(load "~/.opam/default/share/emacs/site-lisp/tuareg-site-file")

;; Open .tex file in auctex mode
(setq TeX-auto-save t)
(setq TeX-parse-self t)
(setq TeX-save-query nil)
;(setq TeX-PDF-mode t)

;; ;; Open .v files with Proof General's Coq mode
;; (load "~/.emacs.d/lisp/PG/generic/proof-site")


(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)

(add-hook 'coq-mode-hook #'company-coq-mode)
(add-hook 'after-change-major-mode-hook (lambda() (electric-indent-mode -1)))


;; Make the % key show the matching parenthesis. If the cursor isn't over a parenthesis, it simply inserts a % like normal.

(global-set-key "%" 'goto-match-paren)

(defun goto-match-paren (arg)
  "Go to the matching parenthesis if on parenthesis, otherwise insert %.
vi style of % jumping to matching brace."
  (interactive "p")
  (cond ((looking-at "\\s\(") (forward-list 1) (backward-char 1))
        ((looking-at "\\s\)") (forward-char 1) (backward-list 1))
        (t (self-insert-command (or arg 1)))))

(require 'flymake)

(defun flymake-get-tex-args (file-name)
(list "pdflatex"
(list "-file-line-error" "-draftmode" "-interaction=nonstopmode" file-name)))

(add-hook 'LaTeX-mode-hook 'flymake-mode)

(setq ispell-program-name "aspell") ; could be ispell as well, depending on your preferences
(setq ispell-dictionary "english") ; this can obviously be set to any language your spell-checking program supports

(add-hook 'LaTeX-mode-hook 'flyspell-mode)
(add-hook 'LaTeX-mode-hook 'flyspell-buffer)

(defun turn-on-outline-minor-mode ()
(outline-minor-mode 1))

(add-hook 'LaTeX-mode-hook 'turn-on-outline-minor-mode)
(add-hook 'latex-mode-hook 'turn-on-outline-minor-mode)
(setq outline-minor-mode-prefix "\C-c \C-o") ; Or something else


(require 'tex-site)
(autoload 'reftex-mode "reftex" "RefTeX Minor Mode" t)
(autoload 'turn-on-reftex "reftex" "RefTeX Minor Mode" nil)
(autoload 'reftex-citation "reftex-cite" "Make citation" nil)
(autoload 'reftex-index-phrase-mode "reftex-index" "Phrase Mode" t)
(add-hook 'latex-mode-hook 'turn-on-reftex) ; with Emacs latex mode
;; (add-hook 'reftex-load-hook 'imenu-add-menubar-index)
(add-hook 'LaTeX-mode-hook 'turn-on-reftex)

(setq LaTeX-eqnarray-label "eq"
LaTeX-equation-label "eq"
LaTeX-figure-label "fig"
LaTeX-table-label "tab"
LaTeX-myChapter-label "chap"
TeX-auto-save t
TeX-newline-function 'reindent-then-newline-and-indent
TeX-parse-self t
TeX-style-path
'("style/" "auto/"
"/usr/share/emacs21/site-lisp/auctex/style/"
"/var/lib/auctex/emacs21/"
"/usr/local/share/emacs/site-lisp/auctex/style/")
LaTeX-section-hook
'(LaTeX-section-heading
LaTeX-section-title
LaTeX-section-toc
LaTeX-section-section
LaTeX-section-label))

(load-library "iso-transl")
;; ## added by OPAM user-setup for emacs / base ## 56ab50dc8996d2bb95e7856a6eddb17b ## you can edit, but keep this line
(require 'opam-user-setup "~/.emacs.d/opam-user-setup.el")
;; ## end of OPAM user-setup addition for emacs / base ## keep this line

;; ## Allow to run iPython shell

(setq python-shell-interpreter "/usr/bin/ipython3"
    python-shell-interpreter-args "--simple-prompt -i")

;; yaml-mode
(require 'yaml-mode)
(setq auto-mode-alist
      (cons '("\\.yml\\'" . yaml-mode)
      (cons '("\\.yaml\\'" . yaml-mode) auto-mode-alist)))

(add-hook 'yaml-mode-hook
	  '(lambda ()
            (define-key yaml-mode-map "\C-m" 'newline-and-indent)))


;; ediff mode
(defun command-line-diff (switch)
  (let ((file1 (pop command-line-args-left))
	(file2 (pop command-line-args-left)))
    (ediff file1 file2)))

(add-to-list 'command-switch-alist '("diff" . command-line-diff))

;; Usage: emacs -diff file1 file2


;; Install ProVerif mode

(setq auto-mode-alist
    (cons '("\\.horn$" . proverif-horn-mode)
    (cons '("\\.horntype$" . proverif-horntype-mode)
    (cons '("\\.pv[l]?$" . proverif-pv-mode)
        (cons '("\\.pi$" . proverif-pi-mode) auto-mode-alist)))))
  (autoload 'proverif-pv-mode "proverif" "Major mode for editing ProVerif code." t)
  (autoload 'proverif-pi-mode "proverif" "Major mode for editing ProVerif code." t)
  (autoload 'proverif-horn-mode "proverif" "Major mode for editing ProVerif code." t)
  (autoload 'proverif-horntype-mode "proverif" "Major mode for editing ProVerif code." t)


;; Install CryptoVerif mode

;; Add opam cryptoverif directory to the load-path
(setq opam-share (substring (shell-command-to-string "opam config var share 2> /dev/null") 0 -1))
(add-to-list 'load-path (concat opam-share "/cryptoverif/emacs"))

(setq auto-mode-alist
      (cons '("\\.cv[l]?$" . cryptoverif-mode)
      (cons '("\\.ocv[l]?$" . cryptoverifo-mode)
      (cons '("\\.pcv$" . pcv-mode) auto-mode-alist))))
(autoload 'cryptoverif-mode "cryptoverif" "Major mode for editing CryptoVerif code." t)
(autoload 'cryptoverifo-mode "cryptoverif" "Major mode for editing CryptoVerif code." t)
(autoload 'pcv-mode "cryptoverif" "Major mode for editing ProVerif and CryptoVerif code." t)


(require 'package)
(add-to-list 'package-archives
             '("melpa-stable" . "https://stable.melpa.org/packages/"))
(package-initialize)


;; magma-mode
(load "~/.emacs.d/lisp/magma-mode.el")
(setq auto-mode-alist
      (append '(("\\.mgm$\\|\\.m$\\|\\.magma$" . magma-mode)) auto-mode-alist))


;; F*-mode
(require 'package)
(add-to-list 'package-archives '("melpa" . "http://melpa.org/packages/") t)
(package-initialize)
(setq auto-mode-alist
      (append '(("\\.fs$\\|\\.fstar$" . fstar-mode)) auto-mode-alist))
